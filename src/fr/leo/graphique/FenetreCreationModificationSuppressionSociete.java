package fr.leo.graphique;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fr.leo.enums.EnumDomaine;
import fr.leo.enums.EnumInteret;
import fr.leo.exceptions.ExceptionAdresseEmail;
import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionChiffreAfaireInferieurZero;
import fr.leo.exceptions.ExceptionNombreEmployes;
import fr.leo.exceptions.ExceptionNumeroTelephone;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.exceptions.ExceptionRatioChiffreAfaireNombreEmployes;
import fr.leo.metier.Adresse;
import fr.leo.metier.Client;
import fr.leo.metier.Prospect;
import fr.leo.metier.Societe;
import fr.leo.utilitaire.ConstantesApplication;
import fr.leo.utilitaire.ListeSocietes;

public class FenetreCreationModificationSuppressionSociete extends JFrame {
	
	/**
	 * Classe qui gere la Modification, la Suppression et la MiseAJour d'une Societe ( Client ou Prospect)
	 */

	// CONSTANTES
	private static final long serialVersionUID = 1L;

	// ATTRIBUTS D'INSTANCE
	private JTextArea txtAreaCommentaires;
	private ListeSocietes listeSocietes;
	private JTextField txtRaisonSociale;
	private JTextField txtTelephone;
	private JTextField txtEmail;
	private JTextField txtNbrEmplyes;
	private JTextField txtChiffreAffaire;
	private JTextField txtDateProspection;
	private JTextField txtVille;
	private JTextField txtCodePostal;
	private JTextField txtNomRue;
	private JTextField txtNumeroRue;
	private JTextField txtInteret;
	private String typeSociete;
	private JLabel lblTitre;
	private JLabel lblDateProspection;
	private JLabel lblInteret;
	private JLabel lblNombreEmployes;
	private JLabel lblChiffreAffaire_1;
	private JLabel lblId;
	private JComboBox<String> comboBxInteret;
	private JComboBox<String> comboBxDomaine;
	private FenetreAccueil fenetreAccueil;
	private JComboBox<Societe> comboBxSocietes; // ComboBox listant les Clients/Prospects
	private JButton btnValiderCreation;
	private JButton btnSuppression;
	private JButton btnModification;
	private String typeCrud;

	// 	*****       		         CONSTRUCTEUR			 	     *******
	/**
	 * 
	 * @param  _listeSocietes ListeSocietes
	 * @param  _fenetreAccueil FenetreAccueil
	 * @param  _typeCrud FenetreAccueil	type d operation Crud
	 */
	public FenetreCreationModificationSuppressionSociete(ListeSocietes _listeSocietes, FenetreAccueil _fenetreAccueil,	String _typeCrud) {

		// Look and feel : Nimbus
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		// AFFECTATION VALEURS ATTRIBUTS D'INSTANCE
		this.fenetreAccueil = _fenetreAccueil;	 			 // reference vers la fenetre Accueil
		this.listeSocietes = _listeSocietes; 				 // reference vers la liste des societes
		this.typeSociete = _fenetreAccueil.getTypeSociete(); // type de Societe selectionnee dans la fenetre Accueil
		this.typeCrud = _typeCrud;							 // type d'operation CRUD selectionnee dans la fenetre Accueil

		// CARACTERISTIQUES GENERALES DE LA JFRAME
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 190, 1200, 570);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		// JPANEL DE LA FRAME
		JPanel pnlCreationSociete = new JPanel();
		pnlCreationSociete.setBounds(0, 0, 1184, 531);
		getContentPane().add(pnlCreationSociete);
		pnlCreationSociete.setLayout(null);
		
		// *****        	 	  LES COMPOSANTS DU PANEL           	   *****

		lblTitre = new JLabel("TITRE :");
		lblTitre.setFont(new Font("Dark Tales", Font.BOLD, 30));
		lblTitre.setBounds(435, 31, 469, 31);
		pnlCreationSociete.add(lblTitre);

		JLabel lblDomaine = new JLabel("Domaine :");
		lblDomaine.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDomaine.setBounds(167, 237, 99, 24);
		pnlCreationSociete.add(lblDomaine);

		txtRaisonSociale = new JTextField();
		txtRaisonSociale.setColumns(10);
		txtRaisonSociale.setBounds(291, 99, 152, 24);
		pnlCreationSociete.add(txtRaisonSociale);

		JLabel lblRaisonSociale = new JLabel("Raison sociale :");
		lblRaisonSociale.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblRaisonSociale.setBounds(167, 99, 99, 24);
		pnlCreationSociete.add(lblRaisonSociale);

		JLabel lblTelephone = new JLabel("Telephone :");
		lblTelephone.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTelephone.setBounds(167, 145, 99, 24);
		pnlCreationSociete.add(lblTelephone);

		txtTelephone = new JTextField();
		txtTelephone.setColumns(10);
		txtTelephone.setBounds(291, 145, 152, 24);
		pnlCreationSociete.add(txtTelephone);

		JLabel lblEmail = new JLabel("Email :");
		lblEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEmail.setBounds(167, 191, 99, 24);
		pnlCreationSociete.add(lblEmail);

		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(291, 191, 152, 24);
		pnlCreationSociete.add(txtEmail);

		lblNombreEmployes = new JLabel("Nombre Employes :");
		lblNombreEmployes.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNombreEmployes.setBounds(167, 333, 114, 24);
		pnlCreationSociete.add(lblNombreEmployes);

		txtNbrEmplyes = new JTextField();
		txtNbrEmplyes.setColumns(10);
		txtNbrEmplyes.setBounds(291, 333, 152, 24);
		pnlCreationSociete.add(txtNbrEmplyes);

		lblChiffreAffaire_1 = new JLabel("Chiffre Affaire :");
		lblChiffreAffaire_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblChiffreAffaire_1.setBounds(167, 285, 99, 24);
		pnlCreationSociete.add(lblChiffreAffaire_1);

		txtChiffreAffaire = new JTextField();
		txtChiffreAffaire.setColumns(10);
		txtChiffreAffaire.setBounds(291, 285, 152, 24);
		pnlCreationSociete.add(txtChiffreAffaire);

		lblDateProspection = new JLabel("Date Prospection :");
		lblDateProspection.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDateProspection.setBounds(605, 285, 114, 24);
		pnlCreationSociete.add(lblDateProspection);

		txtDateProspection = new JTextField();
		txtDateProspection.setColumns(10);
		txtDateProspection.setBounds(731, 285, 152, 24);
		pnlCreationSociete.add(txtDateProspection);

		txtVille = new JTextField();
		txtVille.setColumns(10);
		txtVille.setBounds(731, 237, 152, 24);
		pnlCreationSociete.add(txtVille);

		JLabel lblVille = new JLabel("Ville :");
		lblVille.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblVille.setBounds(605, 237, 99, 24);
		pnlCreationSociete.add(lblVille);

		JLabel lblCodePostal = new JLabel("Code Postal :");
		lblCodePostal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCodePostal.setBounds(605, 191, 99, 24);
		pnlCreationSociete.add(lblCodePostal);

		txtCodePostal = new JTextField();
		txtCodePostal.setColumns(10);
		txtCodePostal.setBounds(731, 191, 152, 24);
		pnlCreationSociete.add(txtCodePostal);

		JLabel lblNomRue = new JLabel("Nom Rue :");
		lblNomRue.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNomRue.setBounds(605, 145, 99, 24);
		pnlCreationSociete.add(lblNomRue);

		txtNomRue = new JTextField();
		txtNomRue.setColumns(10);
		txtNomRue.setBounds(731, 145, 152, 24);
		pnlCreationSociete.add(txtNomRue);

		JLabel lblNumeroRue = new JLabel("Numero Rue :");
		lblNumeroRue.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNumeroRue.setBounds(605, 99, 99, 24);
		pnlCreationSociete.add(lblNumeroRue);

		txtNumeroRue = new JTextField();
		txtNumeroRue.setColumns(10);
		txtNumeroRue.setBounds(731, 99, 152, 24);
		pnlCreationSociete.add(txtNumeroRue);

		lblInteret = new JLabel("Interet");
		lblInteret.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblInteret.setBounds(605, 333, 99, 24);
		pnlCreationSociete.add(lblInteret);

		JLabel lblCommentaires = new JLabel("Commentaires :");
		lblCommentaires.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCommentaires.setBounds(167, 402, 99, 24);
		pnlCreationSociete.add(lblCommentaires);

		txtAreaCommentaires = new JTextArea();
		txtAreaCommentaires.setLineWrap(true);
		txtAreaCommentaires.setRows(3);
		txtAreaCommentaires.setBounds(291, 386, 592, 57);
		pnlCreationSociete.add(txtAreaCommentaires);

		lblId = new JLabel("identifianSociete");
		lblId.setVisible(false);
		lblId.setBounds(42, 429, 46, 14);
		pnlCreationSociete.add(lblId);

		// BOUTON "CREER" : appel a la methode creerSociete()
		btnValiderCreation = new JButton("CREER");
		btnValiderCreation.setBackground(new Color(95, 158, 160));
		btnValiderCreation.setBounds(939, 99, 152, 24);
		pnlCreationSociete.add(btnValiderCreation);
		btnValiderCreation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					creerSociete();
				} catch (ExceptionPersonnalisee e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Creation", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		// BOUTON "SUPPRIMER" : appel a la methode supprimerSociete()
		btnSuppression = new JButton("SUPPRIMER");
		btnSuppression.setBackground(new Color(95, 158, 160));
		btnSuppression.setBounds(939, 99, 152, 24);
		pnlCreationSociete.add(btnSuppression);
		btnSuppression.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				supprimerSociete();
			}
		});

		// BOUTON "MODIFER" : appel a la methode modiferSociete()
		btnModification = new JButton("MODIFER");
		btnModification.setBackground(new Color(95, 158, 160));
		btnModification.setBounds(939, 99, 152, 24);
		pnlCreationSociete.add(btnModification);
		btnModification.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					modiferSociete();
				} catch (ExceptionPersonnalisee e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Modifcation", JOptionPane.WARNING_MESSAGE);
				}
			}
		});

		// BOUTON "QUITTER"
		JButton btnQuitter = new JButton("QUITTER APP");
		btnQuitter.setBounds(939, 191, 152, 24);
		pnlCreationSociete.add(btnQuitter);
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});

		// BOUTON "RETOUR_MENU"
		JButton btnRetourMenu = new JButton("RETOUR MENU");
		btnRetourMenu.setBounds(939, 145, 152, 24);
		pnlCreationSociete.add(btnRetourMenu);
		FenetreCreationModificationSuppressionSociete _this = this;
		btnRetourMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_this.dispose();
				fenetreAccueil.setVisible(true);
			}
		});

		// INSTANCIATION ET ALIMENTATION DE LA COMBO_BOX LISTANT LES "PROSPECT/CLIENT" : 
		String[] data = remplirComboBox(listeSocietes, this.typeSociete);
		comboBxSocietes = new JComboBox(data);
		comboBxSocietes.setBounds(938, 237, 153, 206);
		pnlCreationSociete.add(comboBxSocietes);
		comboBxSocietes.addItemListener(new ItemListenerComboBoxSociete());

		// INSTANCIATION ET ALIMENTATION DE LA COMBO_BOX 'OUI/NON'
		String[] interet = { "OUI", "NON" };
		comboBxInteret = new JComboBox<String>(interet);
		comboBxInteret.setBounds(731, 333, 152, 24);
		pnlCreationSociete.add(comboBxInteret);

		// INSTANCIATION ET ALIMENTATION DE LA COMBO_BOX 'PUBLIC/PRIVE'
		String[] domaine = { "PUBLIC", "PRIVE" };
		comboBxDomaine = new JComboBox<String>(domaine);
		comboBxDomaine.setBounds(291, 237, 152, 24);
		pnlCreationSociete.add(comboBxDomaine);

		// SETTINGS DE LA JFRAME EN FONCTION DE L'OPERATION CRUD REQUISE :
		//  permet de personaliser la fenetre en fonction de l operation CRUD requise,
		//  les labels sont personnalises, certains composants affiches ou non. 
		if (typeCrud.equals(ConstantesApplication.CREATION_SOCIETE)) {
			initialisationSettingsCreationSociete(_fenetreAccueil);
		}
		if (typeCrud.equals(ConstantesApplication.SUPPRESSION_SOCIETE)) {
			initialisationSettingsSuppressionSociete(_fenetreAccueil);
		}
		if (typeCrud.equals(ConstantesApplication.MODIFICATION_SOCIETE)) {
			initialisationSettingsModificationSociete(_fenetreAccueil);
		}
	}
	
	

	// 	*****        		  	   		   GETTERS ET SETTERS      						 *******
	protected JLabel getLblTitre() {
		return lblTitre;
	}
	protected void setLblTitre(JLabel lblTitre) {
		this.lblTitre = lblTitre;
	}
	protected String getTypeSociete() {
		return typeSociete;
	}
	protected void setTypeSociete(String typeSociete) {
		this.typeSociete = typeSociete;
	}
	protected JTextField getTxtDateProspection() {
		return txtDateProspection;
	}
	protected JTextField getTxtInteret() {
		return txtInteret;
	}
	protected JLabel getLblDateProspection() {
		return lblDateProspection;
	}
	protected JLabel getLblInteret() {
		return lblInteret;
	}
	protected JTextField getTxtChiffreAffaire() {
		return txtChiffreAffaire;
	}
	protected void setTxtChiffreAffaire(JTextField txtChiffreAffaire) {
		this.txtChiffreAffaire = txtChiffreAffaire;
	}
	protected JTextField getTxtNbrEmplyes() {
		return txtNbrEmplyes;
	}
	protected void setTxtNbrEmplyes(JTextField txtNbrEmplyes) {
		this.txtNbrEmplyes = txtNbrEmplyes;
	}
	protected JLabel getLblNombreEmployes() {
		return lblNombreEmployes;
	}
	protected void setLblNombreEmployes(JLabel lblNombreEmployes) {
		this.lblNombreEmployes = lblNombreEmployes;
	}
	protected JLabel getLblChiffreAffaire_1() {
		return lblChiffreAffaire_1;
	}
	protected void setLblChiffreAffaire_1(JLabel lblChiffreAffaire_1) {
		this.lblChiffreAffaire_1 = lblChiffreAffaire_1;
	}
	public JComboBox<Societe> getComboBxSocietes() {
		return comboBxSocietes;
	}
	public void setComboBxSocietes(JComboBox<Societe> comboBxSocietes) {
		this.comboBxSocietes = comboBxSocietes;
	}
	protected JComboBox<String> getComboBxInteret() {
		return comboBxInteret;
	}
	protected void setComboBxInteret(JComboBox<String> comboBxInteret) {
		this.comboBxInteret = comboBxInteret;
	}	
	

	
	// 	*****              		  SETTINGS DE LA FENETRE EN FONCTION DU MODE CRUD  REQUIS     				  *******
	
	// MODE CRUD : CREATION
	private void initialisationSettingsCreationSociete(FenetreAccueil _fenetreAccueil) {
		
		setTitle("CREATION");
		this.getLblTitre().setText("CREER " + typeSociete.toUpperCase() + " : ");
		this.getComboBxSocietes().setVisible(false);
		
		if (typeSociete.equals(ConstantesApplication.TYPE_CLIENT)) {
			setExtraFieldsForProspectsVisible(false);
			setExtraFieldsForClientsVisible(true);
		} else if (typeSociete.equals(ConstantesApplication.TYPE_PROSPECT)) {
			setExtraFieldsForProspectsVisible(true);
			setExtraFieldsForClientsVisible(false);
		}
		this.setVisible(true);
	}

	// MODE CRUD : SUPPRESSION
	private void initialisationSettingsSuppressionSociete(FenetreAccueil _fenetreAccueil) {
		setTitle("SUPPRESSION");
		btnValiderCreation.setVisible(false);
		this.getLblTitre().setText("SUPPRIMER " + typeSociete.toUpperCase() + " : ");
		
		if (typeSociete.equals(ConstantesApplication.TYPE_CLIENT)) {
			setExtraFieldsForProspectsVisible(false);
			setExtraFieldsForClientsVisible(true);
		} else if (typeSociete.equals(ConstantesApplication.TYPE_PROSPECT)) {
			setExtraFieldsForProspectsVisible(true);
			setExtraFieldsForClientsVisible(false);
		}
		
		// Rendre les champs du formulaire non modifiables
		lblId.setEnabled(false);
		txtRaisonSociale.setEnabled(false);
		comboBxDomaine.setEnabled(false);
		comboBxInteret.setEnabled(false);
		txtNumeroRue.setEnabled(false);
		txtNomRue.setEnabled(false);
		txtCodePostal.setEnabled(false);
		txtVille.setEnabled(false);
		txtTelephone.setEnabled(false);
		txtEmail.setEnabled(false);
		txtAreaCommentaires.setEnabled(false);
		txtChiffreAffaire.setEnabled(false);
		txtNbrEmplyes.setEnabled(false);
		txtDateProspection.setEnabled(false);
		
		this.setVisible(true);
	}

	// MODE CRUD : MODIFICATION
	private void initialisationSettingsModificationSociete(FenetreAccueil _fenetreAccueil) {
		
		setTitle("MODIFICATION");
		btnValiderCreation.setVisible(false);
		btnSuppression.setVisible(false);
		btnModification.setVisible(true);
		this.getLblTitre().setText("MODIFIER " + typeSociete.toUpperCase() + " : ");

		if (typeSociete.equals(ConstantesApplication.TYPE_CLIENT)) {
			setExtraFieldsForProspectsVisible(false);
			setExtraFieldsForClientsVisible(true);
		} else if (typeSociete.equals(ConstantesApplication.TYPE_PROSPECT)) {
			setExtraFieldsForProspectsVisible(true);
			setExtraFieldsForClientsVisible(false);
		}
		this.setVisible(true);
	}
	// AFFICHAGE/MASQUAGE DES CHAMPS PARTICULIERS PROPRES AUX "PROSPECTS" OU AUX "CLIENTS"
	private void setExtraFieldsForProspectsVisible(boolean bolean) {
		this.getTxtDateProspection().setVisible(bolean);
		this.getComboBxInteret().setVisible(bolean);
		this.getLblDateProspection().setVisible(bolean);
		this.getLblInteret().setVisible(bolean);
	}
	private void setExtraFieldsForClientsVisible(boolean bolean) {
		this.getTxtChiffreAffaire().setVisible(bolean);
		this.getTxtNbrEmplyes().setVisible(bolean);
		this.getLblChiffreAffaire_1().setVisible(bolean);
		this.getLblNombreEmployes().setVisible(bolean);
	}
	

	// 	*****         				     METHODES MODIFIER(), SUPPRIMER() ET CREER() 						   *******
	

	// METHODE MODIFIER_SOCIETE :
	/*
	 * Methode permettant la modification d'une Societe ( un Client ou un Prospect )
	 */
	private void modiferSociete() throws ExceptionPersonnalisee {

		// VARIABLES LOCALES
		String strRaisonSociale;
		EnumDomaine enumDomaine;
		String strNumRue;
		String strNomRue;
		String strCodePostal;
		String strVille;
		String strTelephone;
		String strEmail;
		double dblChiffreAffaire;
		int intNbrEmployes;
		String strCommentaire = "";
		int intIdSociete;
		String strDateProspection;

		// RECUPERATION DES VALEURS COMMUNES AUX CLIENTS ET AUX PROSPECTS, PROVENANT DU FORMULAIRE :
		strRaisonSociale = txtRaisonSociale.getText();
		int intDomaine = comboBxDomaine.getSelectedIndex();
		enumDomaine = intDomaine == 1 ? EnumDomaine.PRIVE : EnumDomaine.PUBLIC;
		strNumRue = txtNumeroRue.getText();
		strNomRue = txtNomRue.getText();
		strCodePostal = txtCodePostal.getText();
		strVille = txtVille.getText();
		strTelephone = txtTelephone.getText();
		strEmail = txtEmail.getText();
		strCommentaire = txtAreaCommentaires.getText();
		intIdSociete = Integer.parseInt(lblId.getText());

		// ******             TRAITEMENT POUR LES  CLIENTS        *******
		if (fenetreAccueil.getTypeSociete().equals(ConstantesApplication.TYPE_CLIENT)) {

			int reponse = JOptionPane.showConfirmDialog(null,
					"Voulez vous vraiment mettre à jour le client " + txtRaisonSociale.getText(), "MISE A JOUR",
					JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
			if (reponse == 0) {

				// RECUPERATION DES VALEURS PROPRES AUX CLIENTS
				try {
					dblChiffreAffaire = Double.parseDouble(txtChiffreAffaire.getText());
					intNbrEmployes = Integer.parseInt(txtNbrEmplyes.getText());
				} catch (NumberFormatException e) {
					throw new ExceptionPersonnalisee("Erreur ChiffreAffaire ET/OU NombreEmployes : " + e.getMessage());
				}

				// MODIFICATION DES ATTRIBUTS DU CLIENT
				for (Societe societe : listeSocietes.getListSocietes()) {
					if (societe.getIdSociete() == intIdSociete) {
						Adresse adrSocieteAdresse = new Adresse(strNumRue, strNomRue, strCodePostal, strVille);
						try {
							societe.setAdresse(adrSocieteAdresse);
							societe.setCommentaires(strCommentaire);
							societe.setDomaine(enumDomaine);
							societe.setEmail(strEmail);
							societe.setRaisonSociale(strRaisonSociale);
							societe.setTelephone(strTelephone);
							((Client) societe).setChiffreAfaire(dblChiffreAffaire);
							((Client) societe).setNbrEmployes(intNbrEmployes);
							JOptionPane.showMessageDialog(null,
									"Le client " + txtRaisonSociale.getText() + " a été mise a jour avec succés",
									"Modification", JOptionPane.INFORMATION_MESSAGE);
						} catch (ExceptionChampObligatoire | ExceptionAdresseEmail | ExceptionNumeroTelephone
								| ExceptionChiffreAfaireInferieurZero | ExceptionNombreEmployes e) {
							System.out.println(e.getMessage());
						}
						reinitialiserChampsFormulaires();
					}
				}
			}
		}
		
		// ******             TRAITEMENT DES PROSPECTS        *******
		if (fenetreAccueil.getTypeSociete().equals(ConstantesApplication.TYPE_PROSPECT)) {

			// RECUPERATION DES VALEURS PROPRES AUX PROSPECTS
			int intInteret = comboBxInteret.getSelectedIndex();
			EnumInteret enumInteret = intInteret == 1 ? EnumInteret.NON : EnumInteret.OUI;
			strDateProspection = txtDateProspection.getText();

			int reponse = JOptionPane.showConfirmDialog(null,
					"Voulez vous vraiment mettre à jour le prospect " + txtRaisonSociale.getText(), "MISE A JOUR",
					JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
			if (reponse == 0) {

				// MODIFICATION DES ATTRIBUTS DU PROSPECT
				for (Societe societe : listeSocietes.getListSocietes()) {
					if (societe.getIdSociete() == intIdSociete) {
						Adresse adrSocieteAdresse = new Adresse(strNumRue, strNomRue, strCodePostal, strVille);
						try {
							societe.setAdresse(adrSocieteAdresse);
							societe.setCommentaires(strCommentaire);
							societe.setDomaine(enumDomaine);
							societe.setEmail(strEmail);
							societe.setRaisonSociale(strRaisonSociale);
							societe.setTelephone(strTelephone);
							((Prospect) societe).setDateProspection(strDateProspection);
							((Prospect) societe).setInteret(enumInteret);
							strDateProspection = txtDateProspection.getText();
							JOptionPane.showMessageDialog(null,
									"Le prospect " + txtRaisonSociale.getText() + " a été mise a jour avec succés",
									"Modification", JOptionPane.INFORMATION_MESSAGE);
						} catch (ExceptionChampObligatoire | ExceptionAdresseEmail | ExceptionNumeroTelephone e) {
							System.out.println(e.getMessage());
						}
						reinitialiserChampsFormulaires();
					}
				}
			}

		}
	}	

	// METHODE SUPPRESSION SOCIETE
	/*
	 * Methode qui supprime une Societe ( un Client ou un Prospect )
	 */
	private void supprimerSociete() {
		
		// Suppression d'un Prospect
		if (typeSociete.equals(ConstantesApplication.TYPE_PROSPECT)) {
			Societe societeASupprimer = null;
			int reponse = JOptionPane.showConfirmDialog(null,
					"Voulez vous vraiment supprimer le prospect " + txtRaisonSociale.getText(), "SUPPRESSION",
					JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
			if (reponse == 0) {
				try {
					// On recupere l'Identifiant du Prospect depuis la JComboBox
					int idSocieteSelectionnee = Integer.parseInt(lblId.getText());
					// On retrouve le Prospect dans la liste a partir de son Identifiant
					societeASupprimer = listeSocietes.chercherSocieteParIdentifant(idSocieteSelectionnee);
				} catch (NumberFormatException e) {
					System.out.println(e.getMessage());
				}
				try {
					// Suppression du Prospect dans la liste des Societe
					listeSocietes.supprimerSociete(societeASupprimer);
					JOptionPane.showMessageDialog(null,
							"Le propect " + txtRaisonSociale.getText() + " a été supprimé avec succés", "Suppression",
							JOptionPane.INFORMATION_MESSAGE);
				} catch (ExceptionPersonnalisee e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Suppression", JOptionPane.INFORMATION_MESSAGE);
				}
				// Reinitialisation des champs du formulaire
				reinitialiserChampsFormulaires();
			}
		}
		// Suppression d'un Client
		if (typeSociete.equals(ConstantesApplication.TYPE_CLIENT)) {
			Societe societeASupprimer = null;
			int reponse = JOptionPane.showConfirmDialog(null,
					"Voulez vous vraiment supprimer le client " + txtRaisonSociale.getText(), "SUPPRESSION",
					JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
			if (reponse == 0) {
				try {
					// On recupere l'Identifiant du Client depuis la JComboBox
					int idSocieteSelectionnee = Integer.parseInt(lblId.getText());
					// On retrouve le Client dans la liste a partir de son Identifiant
					societeASupprimer = listeSocietes.chercherSocieteParIdentifant(idSocieteSelectionnee);
				} catch (NumberFormatException e) {
					System.out.println(e.getMessage());
				}
				try {
					// Suppression du Prospect dans la liste des Societe
					listeSocietes.supprimerSociete(societeASupprimer);
					JOptionPane.showMessageDialog(null,
							"Le client " + txtRaisonSociale.getText() + " a été supprimé avec succés", "Suppression",
							JOptionPane.INFORMATION_MESSAGE);
				} catch (ExceptionPersonnalisee e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Suppression", JOptionPane.INFORMATION_MESSAGE);
				}
				reinitialiserChampsFormulaires();
			}
		}
	}
		
	// METHODE CREATION_SOCIETE
	/*
	 * Permet la creation d'une Societe a partir des valeurs renseignees dans le
	 * formulaire de Creation
	 * Renvoit une/des exeptions de type ExceptionPersonnalisee, si un ou plusieur champs
	 *  ont été incorrectement renseignes
	 * Retourne une reference vers la societe si elle a ete cree
	 * Retourne null si la societe n a pas ete cree
	 */
	private Societe creerSociete() throws ExceptionPersonnalisee {

		// VARIABLES LOCALES
		String strRaisonSociale;
		EnumDomaine enumDomaine;
		String strNumRue;
		String strNomRue;
		String strCodePostal;
		String strVille;
		String strTelephone;
		String strEmail;
		double dblChiffreAffaire;
		int intNbrEmployes;
		String strCommentaire = "";
		Societe societeAcreer = null;

		// RECUPERATION DES VALEURS COMMUNES ( au CLients et aux Prospects) PROVENANT DU FORMULAIRE :
		strRaisonSociale = txtRaisonSociale.getText();
		int intDomaine = comboBxDomaine.getSelectedIndex();
		enumDomaine = intDomaine == 1 ? EnumDomaine.PRIVE : EnumDomaine.PUBLIC;
		strNumRue = txtNumeroRue.getText();
		strNomRue = txtNomRue.getText();
		strCodePostal = txtCodePostal.getText();
		strVille = txtVille.getText();
		strTelephone = txtTelephone.getText();
		strEmail = txtEmail.getText();
		strCommentaire = txtAreaCommentaires.getText();
		
		int reponse = JOptionPane.showConfirmDialog(null,
				"Voulez vous vraiment creer le client " + txtRaisonSociale.getText(), "CREATION",
				JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (reponse == 0) {
			
									// ****      TRAITEMENT DES CLIENTS     ****
			if (fenetreAccueil.getTypeSociete().equals(ConstantesApplication.TYPE_CLIENT)) {
				// RECUPERATION DES VALEURS PROVENANT DU FORMULAIRE, PROPRES AUX CLIENTS  :
				try {
					dblChiffreAffaire = Double.parseDouble(txtChiffreAffaire.getText());
					intNbrEmployes = Integer.parseInt(txtNbrEmplyes.getText());
				} catch (NumberFormatException e) {
					throw new ExceptionPersonnalisee("Erreur ChiffreAffaire ET/OU NombreEmployes : " + e.getMessage());
				}
				// INSTANCIATION DU CLIENT avec les donnees provenant du formulaire
				try {
					societeAcreer = new Client(strRaisonSociale, enumDomaine,
									new Adresse(strNumRue, strNomRue, strCodePostal, strVille), strTelephone, strEmail,
										 dblChiffreAffaire, intNbrEmployes);
				} catch (ExceptionChiffreAfaireInferieurZero | ExceptionNombreEmployes
						| ExceptionRatioChiffreAfaireNombreEmployes | ExceptionNumeroTelephone | ExceptionAdresseEmail
						| ExceptionChampObligatoire e) {
					throw new ExceptionPersonnalisee("Creation du Client impossible car " + e.getMessage());
				}
			}

									// ****      TRAITEMENT DES PROSPECTS     ****
			if (fenetreAccueil.getTypeSociete().equals(ConstantesApplication.TYPE_PROSPECT)) {
				// RECUPERATION DES VALEURS PROVENANT DU FORMULAIRE, PROPRES AUX PROSPECTS,  :
				String strDateProspection = txtDateProspection.getText();
				int intInteret = comboBxInteret.getSelectedIndex();
				EnumInteret enumInteret = intInteret == 0 ? EnumInteret.OUI : EnumInteret.NON;

				// INSTANCIATION PROSPECT avec les donnees provenant du formulaire
				try {
					societeAcreer = new Prospect(strRaisonSociale, enumDomaine,
							new Adresse(strNumRue, strNomRue, strCodePostal, strVille), strTelephone, strEmail,
							strDateProspection, enumInteret);
				} catch (ExceptionNumeroTelephone | ExceptionAdresseEmail | ExceptionChampObligatoire e) {
					throw new ExceptionPersonnalisee("Creation du prospect impossible car " + e.getMessage());
				}
			}

			// AJOUT DE COMMENTAIRES SI LE CHAMPS A ETE RENSEIGNE DANS LE FORMULAIRE
			if (!strCommentaire.equals("")) {
				if (societeAcreer != null) {
					societeAcreer.setCommentaires(strCommentaire);
				}				
			}

			// REINITIALISATION DU CHAMPS DU FORMULAIRE
			if (societeAcreer != null) {
				reinitialiserChampsFormulaires();
			}

			// AJOUT DE LA NOUVEL SOCIETE ( CLIENT ou PROSPECT ) DANS LA LISTE DES SOCIETES
			try {
				listeSocietes.ajouterSociete(societeAcreer);
				JOptionPane.showMessageDialog(null,
						"Le client " + txtRaisonSociale.getText() + " a été créé avec succés", "Modification",
						JOptionPane.INFORMATION_MESSAGE);
			} catch (ExceptionPersonnalisee e) {
				throw new ExceptionPersonnalisee(e.getMessage());
			}
		}
		return societeAcreer;
	}	
	// Methode permettant de reinitialiser les champs du formulaire
	private void reinitialiserChampsFormulaires() {
		// Reinitialisation des champs du formulaire
		lblId.setText("");
		txtRaisonSociale.setText("");
		txtNumeroRue.setText("");
		txtNomRue.setText("");
		txtCodePostal.setText("");
		txtVille.setText("");
		txtTelephone.setText("");
		txtEmail.setText("");
		txtAreaCommentaires.setText("");
		txtChiffreAffaire.setText("");
		txtNbrEmplyes.setText("");
		txtDateProspection.setText("");
	}

	

	// 	*****            	  METHODES POUR LA GESTION DE LA COMBO_BOX DES SOCIETES 			   *******

	// METHODE QUI PREPARE LES DONNEES POUR LA COMBO_BOX_SOCIETES
	/*
	 * Methode qui prepare les donnees pour alimenter la ComBoBoxSociete.
	 * Retourne un tableau de String avec les donnees pour alimenter la ComBoBoxSociete.
	 */
	private String[] remplirComboBox(ListeSocietes listeSocietes, String typeSociete) {
		
		// PREPARATION DES ENTREES POUR LA COMBO_BOX :
		//  Selection des valeurs des champs "idSociete" et "RaisonSociale
		ArrayList<Societe> listeTemp = listeSocietes.getListSocietes();
		String stringTemp = "";

		if (typeSociete.equals(ConstantesApplication.TYPE_CLIENT)) {
			for (Societe societe : listeTemp) {
				if (societe instanceof Client) {
					stringTemp = stringTemp + "::" + societe.getIdSociete() + " " + societe.getRaisonSociale();
				}
			}
		}
		if (typeSociete.equals(ConstantesApplication.TYPE_PROSPECT)) {
			for (Societe societe : listeTemp) {
				if (societe instanceof Prospect) {
					stringTemp = stringTemp + "::" + societe.getIdSociete() + " " + societe.getRaisonSociale();
				}
			}
		}
		String[] tabTempStrings = { "" };
		return tabTempStrings = stringTemp.split("::");
	}
	

	
	// LISTENER ASSOCIE A LA COMBO_BOX_SOCIETE
	/*
	 * Remplit les champs du formulaire avec les valeurs associees a la 
	 *  societe selectionnee dans la ComboBoxSociete
	 */
	class ItemListenerComboBoxSociete implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent e) {

			// Recupere l'IdSociete associé a la Societe selectionnee dans la ComboBox
			int idx;
			String societeSelectionnee = ((JComboBox<?>) e.getSource()).getSelectedItem().toString();
			String[] tabTemp = societeSelectionnee.split(" ");
			String idSocieteASupprimer = tabTemp[0];
			Integer id = Integer.parseInt(idSocieteASupprimer);

			ArrayList<Societe> listeTemp = listeSocietes.getListSocietes();
			
			// GESTION D'UN PROSPECT :
			if (typeSociete.equals(ConstantesApplication.TYPE_PROSPECT)) {
				for (Societe societe : listeTemp) {
					if (societe instanceof Prospect) {
						if (societe.getIdSociete() == id) {
							remplirChampsCommunsSociete(societe);
							txtDateProspection.setText(((Prospect) societe).getDateProspection() );
							idx = ((Prospect) societe).getInteret().equals(EnumInteret.OUI.toString()) ? 0 : 1;
							comboBxInteret.setSelectedIndex(idx);
						}
					}
				}
			}
			// GESTION D'UN CLIENT :
			if (typeSociete.equals(ConstantesApplication.TYPE_CLIENT)) {
				for (Societe societe : listeTemp) {
					if (societe instanceof Client) {
						if (societe.getIdSociete() == id) {
							remplirChampsCommunsSociete(societe);
							txtChiffreAffaire.setText( ((Client) societe).getChiffreAfaire().toString() );
							txtNbrEmplyes.setText(((Client) societe).getNbrEmployes().toString());
						}
					}
				}
			}
		}

		// Remplissage des champs communs aux Prospects et aux Clients
		private void remplirChampsCommunsSociete(Societe societe) {
			int idx;
			lblId.setText(societe.getIdSociete().toString());
			txtRaisonSociale.setText(societe.getRaisonSociale());
			idx = societe.getDomaine().equals(EnumDomaine.PRIVE.toString()) ? 1 : 0;
			comboBxDomaine.setSelectedIndex(idx);
			txtNumeroRue.setText(societe.getAdresse().getNumRue());
			txtNomRue.setText(societe.getAdresse().getNomRue());
			txtCodePostal.setText(societe.getAdresse().getCodePostal());
			txtVille.setText(societe.getAdresse().getVille());
			txtTelephone.setText(societe.getTelephone());
			txtEmail.setText(societe.getEmail());
			// societe.getCommentaires() = societe.getCommentaires() == null ? "" : societe.getCommentaires();
			txtAreaCommentaires.setText(societe.getCommentaires());
		}
	}
}
