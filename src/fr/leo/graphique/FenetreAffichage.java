package fr.leo.graphique;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;

import fr.leo.metier.Client;
import fr.leo.metier.Contrat;
import fr.leo.metier.Prospect;
import fr.leo.metier.Societe;
import fr.leo.utilitaire.ConstantesApplication;
import fr.leo.utilitaire.ListeSocietes;

public class FenetreAffichage extends JFrame {
	
	/**
	 * Classe qui gere l'affichage de tous les Clients ou de tous les Prospects
	 *  selon la selection de l'utilisateur dans la fenetre 'Accueil'
	 * 
	 */

	// CONSTANTES D'INSTANCE
	private static final long serialVersionUID = 1L;

	// ATTRIBUTS D'INSTANCE
	private DefaultTableModel listData; 	// Modele pour la JTable listant les Clients/Prospects
	private String typeSociete = ""; 		// Permet de connaitre le type de societe selectionne dans la FenetreAccueil
	private FenetreAccueil fenetreAccueil;  // Permet d avoir une reference vers la FenetreAccueil
  
	// CONSTRUCTEUR
	public FenetreAffichage(ListeSocietes listeSocietes, FenetreAccueil _fenetreAccueil) {

		// LookAndFell : Nimbus
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		// INITIALISATION DES ATTRIBUTS D'INSTANCE
		this.fenetreAccueil = _fenetreAccueil;
		this.typeSociete = _fenetreAccueil.getTypeSociete();

		// INTIALISATION DES CARACTERISTIQUES GENERALES DE LA JFRAME
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 190, 1200, 570);
		this.setLocationRelativeTo(null);
		setTitle("LISTE ");
		getContentPane().setLayout(null);

		// INITIALISATION DU JPANEL
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1184, 1);
		getContentPane().add(panel);
		panel.setLayout(null);

		// BOUTON RETOUR ACCUEIL : retour vers la fenetre 'Accueil'
		JButton btnRetourMenu = new JButton("RETOUR ACCUEIL");
		btnRetourMenu.setBounds(375, 425, 152, 24);
		this.getContentPane().add(btnRetourMenu);
		FenetreAffichage _this = this;
		btnRetourMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_this.dispose();
				fenetreAccueil.setVisible(true);
			}
		});

		// BOUTON QUITTER APPLICATION : permet de quitter l'application
		JButton btnQuitterApplication = new JButton("QUITTER APPLICATION");
		btnQuitterApplication.setBounds(603, 425, 208, 24);
		getContentPane().add(btnQuitterApplication);
		btnQuitterApplication.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});

		// JLABEL TITRE DE LA FENETRE : 
		//  s'adapte au type de Societe selectionne par l utilisateur (un Client ou un Propect)
		JLabel lblTitreAffichage = new JLabel();
		getContentPane().add(lblTitreAffichage);
		lblTitreAffichage.setFont(new Font("Dark Tales", Font.BOLD, 30));
		lblTitreAffichage.setBounds(541, 21, 423, 32);
		lblTitreAffichage.setText(typeSociete.toUpperCase() + "S : ");

		// INSTANCIATION ET ALIMENTATION DE LA JTABLE 'PROSPECTS/CLIENTS'
		// LA JTABLE EST INSEREE DANS UN JSCROLLPANE
		JScrollPane jScrollPaneSocietes = remplirJTable(listeSocietes, this.typeSociete);

		// AJOUT DU JSCROLLPANE AU CONTENT_PANE
		getContentPane().add(jScrollPaneSocietes);
		
	} // FIN CONSTRUCTEUR
	
	
	

	// *****           		      METHODES            	      *****

	// METHODE REMPLIR_JTABLE :
	/**
	 * Methode qui remplit une JTable soit avec des CLient, soit avec des
	 * Prospects, selon la selection de l'utilisateur dans la fenetre Accueil.
	 * Retourne un JSrcollPane englobant la JTable.
	 * 
	 * @param ListeSocietes  listeSocietes
	 * @param String  typeSociete
	 * @return JScrollPane jScrollPane
	 */
	private JScrollPane remplirJTable(ListeSocietes listeSocietes, String typeSociete) {

		// CREATION DU MODELE ET AJOUT DE LA LIGNE DES ENTETES DE LA JTABLE
		String[] cols = null;
		if (typeSociete.equals(ConstantesApplication.TYPE_CLIENT)) {
			cols = Client.getListeDesDonneesGerees();
		} else {
			cols = Prospect.getListeDesDonneesGerees();
		}
		listData = new DefaultTableModel(cols, 0);

		// INSTANCIATION DE LA JTABLE ET INSERTION DANS UN J_SCROLL_PANE
		JTable jtbl_ListeDesUtilisateur = new JTable(listData);
		jtbl_ListeDesUtilisateur.setEnabled(false);
		JScrollPane panTable = new JScrollPane();
		panTable.setLocation(20, 65);
		panTable.setViewportView(jtbl_ListeDesUtilisateur);
		panTable.setSize(1141, 262);
		
		jtbl_ListeDesUtilisateur.addMouseListener(new MouseAdapter() {	
			@Override
			public void mousePressed(MouseEvent evt) {				
			     JTable source = (JTable)evt.getSource();
		            int row = source.rowAtPoint( evt.getPoint() );
		            int column = source.columnAtPoint( evt.getPoint() );
		            int s= Integer.parseInt(  source.getModel().getValueAt(row, 0) +"" );
		            JOptionPane.showMessageDialog(null, s);	
		            
		            ArrayList<Contrat> c1 =  ((Client) (listeSocietes.chercherSocieteParIdentifant(s))).getListeDesContratsDuClient() ;
		            for (Contrat contrat : c1) {
						System.out.println(contrat);
					}
			}
		});

		// AJOUT DES ENTREES AU MODELE :
		//  soit des Clients, ou soit des Prospects, selon la selection de  l'utilisateur
		ArrayList<Societe> listeTemp = listeSocietes.getListSocietes();
		for (Societe societe : listeTemp) {
			// Champs communs aux Clients et Prospects
			String stringChampsCommuns = societe.getIdSociete() + "::" + societe.getRaisonSociale() + "::"
											+ societe.getDomaine() + "::" + societe.getAdresse().getNumRue() + "::"
											+ societe.getAdresse().getNomRue() + "::" + societe.getAdresse().getCodePostal() + "::"
											+ societe.getAdresse().getVille() + "::" + societe.getTelephone() + "::" 
											+ societe.getEmail();
						// Traitement special si le commentaite a une valeur egale a 'null'
						String tempCommentaire = societe.getCommentaires() == null ? "" : societe.getCommentaires() ;
						stringChampsCommuns = stringChampsCommuns + "::" + tempCommentaire;
			// Ajout des champs particuliers aux Clients 
			if (typeSociete.equals(ConstantesApplication.TYPE_CLIENT)) {
				if (societe instanceof Client) {
					String stringTemp = stringChampsCommuns + "::" + ((Client) societe).getChiffreAfaire() + "::" + ((Client) societe).getNbrEmployes();
					String[] tabTempStrings = stringTemp.split("::");
					listData.addRow(tabTempStrings);
				}
			}
			// Ajout des champs particuliers aux Prospects
			else if (typeSociete.equals(ConstantesApplication.TYPE_PROSPECT)) {
				if (societe instanceof Prospect) {
					String stringTemp = stringChampsCommuns  + "::" + ((Prospect) societe).getDateProspection() + "::"
							+ ((Prospect) societe).getInteret();
					String[] tabTempStrings = stringTemp.split("::");
					listData.addRow(tabTempStrings);
				}
			}
		}
		return panTable; // retourne un JscrollPane contenant la Jtable qui liste les Prospects/Clients
	}
}
