package fr.leo.graphique;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fr.leo.utilitaire.ConstantesApplication;
import fr.leo.utilitaire.ListeSocietes;

public class FenetreAccueil extends JFrame {

	/**
	 * Fenetre d'accueil de l'application REVERSO
	 */
	
	// CONSTANTES DE CLASSE
	private static final long serialVersionUID = 1L;
	
	// ATTRIBUTS D'INSTANCE
	private ListeSocietes listeSocietes;
	private String typeSociete;
	private JLabel lblTitrePanelCrud;
	private JButton btnContrats = new JButton();
	
	// CONSTRUCTEUR
	public FenetreAccueil(ListeSocietes _listeSocietes) {	
		
		// LookAndFeel "Nimbus"
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		// AFFECTATION DES ATTRIBUTS D'INSTANCE
		this.listeSocietes = _listeSocietes;
		typeSociete = ConstantesApplication.TYPE_CLIENT;
		
		// CARACTERISTIQUES GENERALES DE LA JFRAME
		setTitle("GESTION DES CLIENTS ET DES PROSPECTS");	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 190, 1200, 570);		
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		

		// *****             CREATION DES PANNEAUX : "ACCUEIL_APPLICATION" ET "CRUD"         *****
		// PANEL "ACCUEIL APPLICATION"
		JPanel jpnAccueil = new JPanel();
		jpnAccueil.setBounds(0, 0, 1184, 300);
		getContentPane().add(jpnAccueil);
		jpnAccueil.setLayout(null);
		// PANEL "CRUD" : Creation, Suppression, MiseAJour
		JPanel jplCrud = new JPanel();
		jplCrud.setVisible(false);
		jplCrud.setBounds(0, 0, 1184, 300);
		getContentPane().add(jplCrud);
		jplCrud.setLayout(null);
		
		

		// *****              COMPOSANTS DU PANNEL "ACCUEIL_APPLICATION"         ***** 
	
		// LABEL "TITRE"
		JLabel lblTitrePanelAccueil = new JLabel("GESTION CLIENTS/PROSPECTS :");
		lblTitrePanelAccueil.setToolTipText("Gestion des clients et des prospects");
		lblTitrePanelAccueil.setFont(new Font("Dark Tales", Font.BOLD, 30));
		lblTitrePanelAccueil.setBounds(387, 38, 564, 60);
		jpnAccueil.add(lblTitrePanelAccueil);	
		
		// BOUTON "CLIENTS" : panel CRUD visible, panel ACCUEIL_APPLICATION invisible
		JButton bntClients = new JButton("CLIENTS");
		bntClients.setToolTipText("Gestion des clients");
		bntClients.setBackground(new Color(46, 139, 87));
		bntClients.setBounds(277, 204, 204, 43);
		jpnAccueil.add(bntClients);
		bntClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				typeSociete = ConstantesApplication.TYPE_CLIENT;
				lblTitrePanelCrud.setText(typeSociete.toUpperCase() + "S :");
				jplCrud.setVisible(true);
				jpnAccueil.setVisible(false);
				btnContrats.setVisible(true);
			}
		});
		
		// BOUTON "PROSPECTS" : panel CRUD visible, panel ACCUEIL_APPLICATION invisible
		JButton bntProspects = new JButton("PROSPECTS");
		bntProspects.setToolTipText("Gestion des prospects");
		bntProspects.setBounds(781, 202, 204, 47);
		jpnAccueil.add(bntProspects);
		bntProspects.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				typeSociete = ConstantesApplication.TYPE_PROSPECT;
				lblTitrePanelCrud.setText(typeSociete.toUpperCase() + "S: ");
				jplCrud.setVisible(true);				
				jpnAccueil.setVisible(false);
				btnContrats.setVisible(false);
			}
		});

		
		// *****                  COMPOSANTS DU PANNEL "CRUD"                 *****
		
		// LABEL TITRE
		lblTitrePanelCrud = new JLabel();
		lblTitrePanelCrud.setBounds(500, 38, 250, 42);
		lblTitrePanelCrud.setFont(new Font("Tekton Pro", Font.BOLD, 30));
		jplCrud.add(lblTitrePanelCrud);
		
		// BOUTON AFICHAGE : appel a la methode 'afficherSocietes()'
		JButton btnAffichage = new JButton("AFFICHAGE");
		btnAffichage.setToolTipText("Affiche la liste");
		btnAffichage.setBounds(265, 95, 140, 42);
		jplCrud.add(btnAffichage);
		btnAffichage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afficherSocietes();
			}
		});
		
		// BOUTON MODIFICATION : appel a la methode 'modiferSociete()'
		JButton btnModification = new JButton("MODIFICATION");
		btnModification.setToolTipText("Modifier une societe");
		btnModification.setBounds(436, 95, 140, 42);
		jplCrud.add(btnModification);
		btnModification.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modiferSociete();
			}
		});
		
		// BOUTON SUPPRESSION : appel a la methode 'supprimerSociete()'
		JButton btnSuppression = new JButton("SUPPRESSION");
		btnSuppression.setToolTipText("Supprimer une societe");
		btnSuppression.setBounds(608, 95, 140, 42);
		jplCrud.add(btnSuppression);
		btnSuppression.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				supprimerSociete();
			}
		});
		
		// BOUTON CREATION : appel a la methode 'creerSociete()'
		JButton btnCreation = new JButton("CREATION");
		btnCreation.setToolTipText("Creer une societe");
		btnCreation.setBounds(792, 95, 140, 42);
		jplCrud.add(btnCreation);
		btnCreation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				creerSociete();
			}
		});
		
		// BOUTON CONTRATS : 
		btnContrats = new JButton("CONTRATS");
		btnContrats.setToolTipText("Contrats Client");
		btnContrats.setBounds(532, 185, 140, 42);
		jplCrud.add(btnContrats);
		btnContrats.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				voirContrats();
			}
		});
		
		if (typeSociete == ConstantesApplication.TYPE_CLIENT) {
			btnContrats.setVisible(false);
		}

		// BOUTON RETOUR_ACCUEIL :  panel CRUD invisible, panel ACCUEIL_APPLICATION visible
		JButton btnRetourAccueil = new JButton("RETOUR ACCUEIL");
		btnRetourAccueil.setToolTipText("Retour fenetre accueil");
		btnRetourAccueil.setBounds(192, 185, 140, 42);
		jplCrud.add(btnRetourAccueil);
		btnRetourAccueil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				jplCrud.setVisible(false);
				jpnAccueil.setVisible(true);
			}
		});
		
		// BOUTON QUITTER
		JButton btnQuitter = new JButton("QUITTER APP");
		btnQuitter.setToolTipText("Quitter l'application");
		btnQuitter.setBounds(869, 185, 140, 42);
		jplCrud.add(btnQuitter);
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});	
	} // FIN CONSTRUCTEUR
	
	
	 
	// *****           		      GETTERS ET SETTERS            	      *****
	protected String getTypeSociete() {
		return typeSociete;
	}
	
	
	// *****           		           METHODES                    	      *****
	// AFFICHER_SOCIETE
	private void afficherSocietes() {	
		this.setVisible(false);
		new FenetreAffichage(listeSocietes, this ).setVisible(true);
	}
	// MODIFIER_SOCIETE 
	private void modiferSociete( ) {
		this.setVisible(false);
		new FenetreCreationModificationSuppressionSociete(listeSocietes, this, ConstantesApplication.MODIFICATION_SOCIETE);	
	}
	// CREER_SOCIETE
	private void creerSociete() {
		this.setVisible(false);
		new FenetreCreationModificationSuppressionSociete(listeSocietes , this, ConstantesApplication.CREATION_SOCIETE);
	}
	// SUPPRIMER_SOCIETE
	private void supprimerSociete() {	
		this.setVisible(false);
		new FenetreCreationModificationSuppressionSociete(listeSocietes , this, ConstantesApplication.SUPPRESSION_SOCIETE);
	}
	// CONTRATS
	private void voirContrats() {	
		this.setVisible(false);
		new FenetreContrats(listeSocietes , this, ConstantesApplication.SUPPRESSION_SOCIETE);
	}	
}
