package fr.leo.exceptions;

public class ExceptionRatioChiffreAfaireNombreEmployes extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Classe permettant de gerer les clients n'ayant pas un
	 * ration chiffreAfaire/NombreEmployes superieur a 10.
	 * Le ratio chiffreAfaire/NombreEmployes doit etre superieur a  10
	 * pour pouvoir instancie  un nouveau client.
	 * 
	 * @param  message String
	 */

	public ExceptionRatioChiffreAfaireNombreEmployes(String message){
		super(message);
	}

}
