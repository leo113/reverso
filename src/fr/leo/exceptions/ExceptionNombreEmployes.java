package fr.leo.exceptions;

public class ExceptionNombreEmployes extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Classe permettant de gerer les clients n'ayant aucun employe
	 * lors de l'instanciation d'une nouveau Client.
	 * Un Client doit obligatoirement avoir aumoins 1 employe.
	 * 
	  * @param  message String
	 */

	public ExceptionNombreEmployes(String message){
		super(message);
	}

}
