package fr.leo.exceptions;

public class ExceptionPersonnalisee extends Exception {	
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Classe permettant de gerer une exception personalisee
	 * 
	 * @param  message String
	 */

	public ExceptionPersonnalisee(String message){
		super(message);
	}

}
