package fr.leo.exceptions;

public class ExceptionChiffreAfaireInferieurZero extends Exception {	
	
	/**
	 * Classe permettant de  gerer les chiffres d'afaire non positif
	 * lors de l'instanciation d'une nouveau Client
	 * Un Client doit obligatoirement avoir un chiffre d'afaire superieur
	 * ou egal a zero pour pouvoir etre instancie
	 * 
	 * @param  message String
	 */

	private static final long serialVersionUID = 1L;

	public ExceptionChiffreAfaireInferieurZero(String message){
		super(message);
	}

}
