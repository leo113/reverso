package fr.leo.exceptions;

public class ExceptionNumeroTelephone extends Exception {	

	private static final long serialVersionUID = 1L;

	/**
	 * Classe permettant de  gerer les clients n'ayant pas un
	 * numero de telephone correct.
	 * Le numero de telephone doit matcher la Regex suivante : "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b"
	 * 
	 * @param  message String
	 */

	public ExceptionNumeroTelephone(String message){
		super(message);
	}

}
