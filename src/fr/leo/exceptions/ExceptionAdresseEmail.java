package fr.leo.exceptions;

public class ExceptionAdresseEmail extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Classe permettant de gerer les clients n'ayant pas une
	 * adresse email correcte.
	 * L'adresse email doit matcher la Regex suivante : "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b"
	 * 
	 * @param  message String
	 */

	public ExceptionAdresseEmail(String message){
		super(message);
	}
}
