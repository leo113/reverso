package fr.leo.exceptions;

public class ExceptionChampObligatoire extends Exception {
	
	/**
	 * Classe permettant de gerer l'absence de valeur pour un champs obligatoire 
	 * 
	 * @param  message String
	 */

	private static final long serialVersionUID = 1L;

	public ExceptionChampObligatoire(String message){
		super(message);
	}

}
