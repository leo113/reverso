package fr.leo.enums;

import fr.leo.metier.Societe;

public enum EnumDomaine {
	
	/**
	 * Domaines possibles pour une Societe
	 * @see Societe
	 */
	
	PRIVE, PUBLIC
}
