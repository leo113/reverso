package fr.leo.main;
import java.awt.EventQueue;
import java.time.LocalDate;
import java.util.Date;

import fr.leo.enums.EnumDomaine;
import fr.leo.enums.EnumInteret;
import fr.leo.exceptions.ExceptionAdresseEmail;
import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionChiffreAfaireInferieurZero;
import fr.leo.exceptions.ExceptionNombreEmployes;
import fr.leo.exceptions.ExceptionNumeroTelephone;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.exceptions.ExceptionRatioChiffreAfaireNombreEmployes;
import fr.leo.graphique.FenetreAccueil;
import fr.leo.metier.Adresse;
import fr.leo.metier.Client;
import fr.leo.metier.Contrat;
import fr.leo.metier.Prospect;
import fr.leo.utilitaire.ListeSocietes;

public class Main {

	public static void main(String[] args) throws ExceptionRatioChiffreAfaireNombreEmployes, ExceptionNombreEmployes {
		
		// CREATION LISTE SOCIETE
		ListeSocietes listeSocietes = new ListeSocietes();
		
		// CREATION DE CONTRATS POUR CERTAINS CLIENTS
		Contrat c1 = new Contrat(1, "Contrat1", 100.2 , LocalDate.of(2015, 01, 14), LocalDate.of(2019, 01, 14));
		Contrat c2 = new Contrat(2, "Contrat2", 200.2 , LocalDate.of(2015, 02, 14), LocalDate.of(2019, 02, 14));
		Contrat c3 = new Contrat(3, "Contrat3", 300.2 , LocalDate.of(2015, 03, 14), LocalDate.of(2019, 03, 14));
		Contrat c4 = new Contrat(4, "Contrat4", 400.2 , LocalDate.of(2015, 04, 14), LocalDate.of(2019, 04, 14));
		Contrat c5 = new Contrat(5, "Contrat5", 500.2 , LocalDate.of(2015, 05, 14), LocalDate.of(2019, 05, 14));
		
		// CREATION DE PLUSIEURS CLIENTS
		Client client1 = null;
		Client client2 = null;
		Client client3 = null;
		Client client4 = null;
		Client client5 = null;
		try {
			client1 = new Client("FleurAuDent", EnumDomaine.PRIVE, new Adresse("10", "rue des jardins", "57120", "Metz"), "00331254236587", "bartabac@free.fr", 12145121, 100);
			client2 = new Client("Eoles", EnumDomaine.PUBLIC, new Adresse("11", "rue des alouettes", "75002", "Paris"), "00332545658745", "eole.la@free.fr", 1112201.3, 1247);
			client3 = new Client("Oceanos", EnumDomaine.PRIVE, new Adresse("12", "rue des prairies en fleur", "29800", "Landernau"), "00333625148974", "oceanos.lia@free.fr", 11110, 1110);
			client4 = new Client("Maia", EnumDomaine.PRIVE, new Adresse("13", "rue des vertes collines", "59000", "Lille"), "00335554736524", "maia.lea@free.fr", 11241110, 14);
			client5 = new Client("Chloris", EnumDomaine.PUBLIC, new Adresse("14", "rue des coquelicots", "64200", "Biarritz"), "00333521654782", "chloris.li@free.it", 11110, 1110);
		} catch (ExceptionChiffreAfaireInferieurZero e) {
			System.out.println(e.getMessage());
		} catch (ExceptionNombreEmployes e) {
			System.out.println(e.getMessage());
		}catch (ExceptionRatioChiffreAfaireNombreEmployes e) {
			System.out.println(e.getMessage());
		} catch (ExceptionNumeroTelephone e) {
			System.out.println(e.getMessage());
		} catch (ExceptionAdresseEmail e) {
			System.out.println(e.getMessage());
		}catch (ExceptionChampObligatoire e) {
			System.out.println(e.getMessage());
		} catch (ExceptionPersonnalisee e) {
			System.out.println(e.getMessage());
		}
		
		// AJOUT DE CONTRATS A CERTAINS CLIENTS
		try {
			client1.addContratClient(c1);
			client1.addContratClient(c2);
			client1.addContratClient(c3);
			client3.addContratClient(c4);
			client5.addContratClient(c5);
		} catch (ExceptionPersonnalisee e2) {
			System.out.println(e2.getMessage());
		}

		// CREATION DE PLUSIEURS PROSPECTS
		Prospect prospect1 = null;
		Prospect prospect2 = null;
		Prospect prospect3 = null;
		Prospect prospect4 = null;
		Prospect prospect5 = null;
		try {
			prospect1 = new Prospect("Demeter", EnumDomaine.PRIVE, new Adresse("15", "impasse de la ville", "Avignon", "Avignon"), "00332154784523", "demeter.ese@free.fr", "12/12/2000", EnumInteret.NON);
			prospect2 = new Prospect("Themis", EnumDomaine.PUBLIC, new Adresse("16", "rue des mouettes", "37400", "Amboise"), "00333255145587", "themis@free.fr", "14/11/2001", EnumInteret.OUI);
			prospect3 = new Prospect("Hygie", EnumDomaine.PUBLIC, new Adresse("17", "rue du phare", "87100", "Limoges"), "00335421569854", "hygie@free.fr", "23/07/2014", EnumInteret.OUI);
			prospect4 = new Prospect("Boree", EnumDomaine.PRIVE, new Adresse("18", "rue des champs de ble", "17200", "Royan"), "00335422126985", "boree@free.fr", "30/01/2010", EnumInteret.NON);
			prospect5 = new Prospect("Vesta", EnumDomaine.PUBLIC, new Adresse("19", "rue du mais transgenique", "74100", "Chamonix"), "00335215488746", "vesta@free.fr", "12/12/2012", EnumInteret.OUI);		
		} catch (ExceptionNumeroTelephone | ExceptionAdresseEmail | ExceptionChampObligatoire e1) {
			System.out.println(e1.getMessage());
		} catch (ExceptionPersonnalisee e) {
			System.out.println(e.getMessage());
		}
		
		// AJOUT A LA LISTE DES SOCIETES DES PROSPECTS ET DES CLIENTS
		try {
			listeSocietes.ajouterSociete(client1);
			listeSocietes.ajouterSociete(client2);
			listeSocietes.ajouterSociete(client3);
			listeSocietes.ajouterSociete(client4);
			listeSocietes.ajouterSociete(client5);
			listeSocietes.ajouterSociete(prospect1);
			listeSocietes.ajouterSociete(prospect2);
			listeSocietes.ajouterSociete(prospect3);
			listeSocietes.ajouterSociete(prospect4);
			listeSocietes.ajouterSociete(prospect5);
		} catch (ExceptionPersonnalisee e1) {
			System.out.println(e1.getMessage());
		}
		
		// AJOUT DE COMMENTAIRES POUR 1 PROSPECT ET 1 CLIENT
		client1.setCommentaires("Ceci est le commentaire FleurAuDent");
		prospect1.setCommentaires("Ceci est le commentaire Demeter");
		
		// INSTANCIATION DE LA JFRAME D'ACCUEIL
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FenetreAccueil frame = new FenetreAccueil(listeSocietes);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

