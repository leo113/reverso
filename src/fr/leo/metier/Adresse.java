package fr.leo.metier;

import fr.leo.exceptions.ExceptionPersonnalisee;

public class Adresse {
	
	/**
	 * Classe permettant de creer une Adresse pour un Prospect ou  un Client
	 * @see Prospect
	 * @see Client
	 */
	
	// ATTRIBUTS D'INSTANCE
	private String nomRue;
	private String numRue;
	private String codePostal;
	private String ville;	
	
	// CONSTRUCTEUR
	/**
	 * Envoit une exception de type ExceptionPersonnalisee si un des champs
	 *  a pour valeur 'null' ou 'chaine vide' 
	 * @param numRue String 
	 * @param nomRue String 
	 * @param codePostal String 
	 * @param ville String
	 * @throws ExceptionPersonnalisee renvoit une exception si un des champs a une valeur incorrecte
	 */
	public Adresse( String numRue, String nomRue,  String codePostal, String ville) throws ExceptionPersonnalisee {
		super();
		this.setNomRue(nomRue); 
		this.setNumRue(numRue);
		this.setCodePostal(codePostal);
		this.setVille(ville);
	}
	
	// GETTERS ET SETTERS
	public String getNomRue() {
		return nomRue;
	}
	/**
	 * Retourne une exception ExceptionPersonnalisee si le nom de la rue est null
	 *  ou si le nom de la Rue est une chaine de caractere vide
	 * @param nomRue String
	 * @throws ExceptionPersonnalisee
	 */
	protected void setNomRue(String nomRue) throws ExceptionPersonnalisee {
		if ( nomRue==null || nomRue.equals("")) {
			throw new ExceptionPersonnalisee("Le nom de la rue est obligatoire");
		}else{
			this.nomRue = nomRue;			
		}
	}
	
	public String getNumRue() {
		return numRue;
	}
	/**
	 * Retourne une exception ExceptionPersonnalisee si le numéro de la Rue est null
	 *  ou si le numéro de la Rue est une chaine de caractere vide
	 * @param numRue String
	 * @throws ExceptionPersonnalisee
	 */
	protected void setNumRue(String numRue) throws ExceptionPersonnalisee {
		if ( numRue==null || numRue.equals("")) {
			throw new ExceptionPersonnalisee("Le numero de la rue est obligatoire");
		}else{
			this.numRue = numRue;	
		}
	}
	
	public String getCodePostal() {
		return codePostal;
	}	
	/**
	 * Retourne une exception ExceptionPersonnalisee si le Code Postal est null
	 *  ou si le Code Postal est une chaine de caractere vide
	 * @param codePostal String
	 * @throws ExceptionPersonnalisee
	 */
	protected void setCodePostal(String codePostal) throws ExceptionPersonnalisee {
		if ( codePostal==null || codePostal.equals("")) {
			throw new ExceptionPersonnalisee("Le code postal est obligatoire");
		}else{
			this.codePostal = codePostal;	
		}
	}
	
	public String getVille() {
		return ville;
	}
	/**
	 * Retourne une exception ExceptionPersonnalisee si la Ville Postal est null
	 *  ou si la Ville est une chaine de caractere vide
	 * @param ville String
	 * @throws ExceptionPersonnalisee
	 */
	protected void setVille(String ville) throws ExceptionPersonnalisee {
		if (ville==null ||  ville.equals("")) {
			throw new ExceptionPersonnalisee("La ville est obligatoire");
		}else{
			this.ville = ville;	
		}
	}
	
	// TO_STRING
	@Override
	public String toString() {
		return this.numRue + " " + this.nomRue + " " + this.codePostal + " " + this.ville;
	}
}
