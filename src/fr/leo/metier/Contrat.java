package fr.leo.metier;

import java.time.LocalDate;

public class Contrat {
	
	/**
	 * Classe de gestion des Contrats
	 * Un Client a 0 ou n contrats
	 */
	
	// ATTRIBUTS D'INSTANCE
	private int idContrat;
	private String libelleContrat;
	private Double montantContrat;
	private LocalDate dateDebutContrat;
	private LocalDate dateFinContrat;
	
	// CONSTRUCTEURS
	public Contrat() {
	}
	
	public Contrat(int idContrat, String libelleContrat, Double montantContrat, LocalDate dateDebutContrat,
			LocalDate dateFinContrat) {
		super();
		this.setIdContrat(idContrat);
		this.setLibelleContrat(libelleContrat);
		this.setMontantContrat(montantContrat);
		this.setDateDebutContrat(dateDebutContrat);
		this.setDateFinContrat(dateFinContrat);
	}

	// GETTERS ET SETTERS
	public int getIdContrat() {
		return idContrat;
	}
	public void setIdContrat(int idContrat) {
		this.idContrat = idContrat;
	}
	public String getLibelleContrat() {
		return libelleContrat;
	}
	public void setLibelleContrat(String libelleContrat) {
		this.libelleContrat = libelleContrat;
	}
	public Double getMontantContrat() {
		return montantContrat;
	}
	public void setMontantContrat(Double montantContrat) {
		this.montantContrat = montantContrat;
	}
	public LocalDate getDateDebutContrat() {
		return dateDebutContrat;
	}
	public void setDateDebutContrat(LocalDate dateDebutContrat) {
		this.dateDebutContrat = dateDebutContrat;
	}
	public LocalDate getDateFinContrat() {
		return dateFinContrat;
	}
	public void setDateFinContrat(LocalDate dateFinContrat) {
		this.dateFinContrat = dateFinContrat;
	}
	
	@Override
	public String toString() {
		return "Contrat: " 	+ this.getIdContrat() 
							+ " " + this.libelleContrat
							+ " " + this.getMontantContrat() 
							+ "Date de début : " + this.getDateDebutContrat()
							+ "Date de fin : " + this.getDateFinContrat();
	}
	
	

}
