package fr.leo.metier;

import fr.leo.enums.EnumDomaine;
import fr.leo.enums.EnumInteret;
import fr.leo.exceptions.ExceptionAdresseEmail;
import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionNumeroTelephone;
import fr.leo.exceptions.ExceptionPersonnalisee;

public class Prospect extends Societe{
	
	/**
	 * Classe de gestion des Prospects 
	 * C'est une sous classe concrete de la classe abstraite Societe 
	 * @see Societe
	 */

	// ATTRIBUTS DE CLASSE
	private static String[] listeDesDonneesGerees = {"idSociete", "raisonSociale", "domaine", "numRue" , "nomRue" ,
			"codePostal", "ville", "telephone", "email", "commentaire", "dateProspection", "interet" } ;

	// ATTRIBUTS D'INSTANCE
	private String dateProspection;
	private EnumInteret interet;

	// CONSTRUCTEUR
	/**
	 * 
	 * Envoit une exception de type ExceptionNumeroTelephone 	si le numero de telephone est incorrect
	 * Envoit une exception de type ExceptionAdresseEmail 		si l adresse email est incorrecte
	 * Envoit une exception de type ExceptionChampObligatoire	si un des 4 champs n'est pas renseigné
	 * Envoit une exception de type ExceptionPersonnalisee		si la date de prospection incorrecte
	 * @param raisonSociale String  
	 * @param domaine EnumDomaine  (public ou prive)
	 * @param adresse Adresse 
	 * @param telephone String 
	 * @param email String 
	 * @param dateProspection String 
	 * @param interet String  (oui ou  non)
	 * @throws ExceptionNumeroTelephone exceptionNumeroTelephone
	 * @throws ExceptionAdresseEmail exceptionAdresseEmail
	 * @throws ExceptionChampObligatoire exceptionChampObligatoire
	 * @throws ExceptionPersonnalisee exceptionPersonnalisee
	 * @see ExceptionNumeroTelephone
	 * @see ExceptionAdresseEmail
	 * @see ExceptionChampObligatoire
	 * @see ExceptionPersonnalisee
	 */
	public Prospect(String raisonSociale, EnumDomaine domaine, Adresse adresse,
					String telephone, String email, String dateProspection, EnumInteret interet)
			throws ExceptionNumeroTelephone, ExceptionAdresseEmail, ExceptionChampObligatoire, ExceptionPersonnalisee {
		super(raisonSociale, domaine, adresse, telephone, email);
		this.setInteret(interet);
		this.setDateProspection(dateProspection);
		this.setAdresse(adresse);
		Societe.setNbrInstancesDeSociete();
		this.setIdSociete(Societe.getNbrInstancesDeSociete());
	}

	// GETTERS ET SETTERS
	public String getDateProspection() {
		return dateProspection;
	}
	

	/**
	 * Retourne une excption de type ExceptionPersonnalisee si la date de prospection
	 *  ne matche pas avec la Regex : [0-9]{2}\\/[0-9]{2}\\/[0-9]{4}
	 *  La date doit etre du type : 21/01/2014
	 * @param dateProspection String 
	 * @throws ExceptionPersonnalisee exceptionPersonnalisee
	 * @see ExceptionPersonnalisee
	 */
	public void setDateProspection(String dateProspection) throws ExceptionPersonnalisee {			
	    if ( (dateProspection==null) || (dateProspection.isEmpty())) {
	        throw new ExceptionPersonnalisee("La date de prospection doit etre renseignee");
	    }else   if (!(dateProspection.matches("[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}"))){
	    	throw new ExceptionPersonnalisee("Date de prospection incorrecte");
	    }else{
	    	this.dateProspection = dateProspection;
	    }
	}
	public EnumInteret getInteret() {
		return interet;
	}
	public void setInteret(EnumInteret interet) {
		this.interet = interet;
	}
	
	/**
	 * Retourne la liste des donnees caracterisants un Prospect
	 *  et gerees par la classe
	 * @return String[] listeDesDonneesGerees
	 */
	public static String[] getListeDesDonneesGerees() {
		return listeDesDonneesGerees;
	}
	
	// TO_STRING
	@Override
	public String toString() {
		return super.toString() + " " + this.getDateProspection() + " " + this.getInteret() +  + this.getIdSociete() ;
	}
}
