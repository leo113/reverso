package fr.leo.metier;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.leo.enums.EnumDomaine;
import fr.leo.exceptions.ExceptionAdresseEmail;
import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionNumeroTelephone;

public abstract class Societe {	
	
	/**
	 * Classe abstraite
	 * Permet de gerer des Clients et de Prospects
	 * @see Prospect
	 * @see Client
	 */
	
	// Constantes
	private static final String VALID_EMAIL_ADDRESS_REGEX = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b";
	private static final String VALID_NUM_TEL_REGEX = "(0|\\\\+33|0033)[1-9][0-9]{8}";

	// ATTRIBUTS DE CLASSE
	protected static Integer nbrInstancesDeSociete = 0;
	private static String[] listeDesDonneesGerees = {"idSociete", "raisonSociale", "domaine", "numRue" , "nomRue" ,
			"codePostal", "ville", "telephone", "email", "commentaire" };

	// ATRIBUTS D'INSTANCE
	private String raisonSociale;
	private EnumDomaine domaine;
	private Adresse adresse;
	private String telephone;
	private String email;
	private String commentaires;	
	private int idSociete;

	// CONSTRUCTEUR
	/**
	 * Envoit une exception de type ExceptionNumeroTelephone si le numero de  de telephone est incorrect
	 * Envoit une exception de type ExceptionAdresseEmail si l adresse email est incorrecte
	 * Envoit une exception de type ExceptionChampObligatoire si un des 4 champs n'est pas renseigné
	 * @param  raisonSociale Sting
	 * @param  domaine EnumDomaine (public ou prive)
	 * @param  adresse Adresse
	 * @param  telephone String
	 * @param  email String
	 * @see ExceptionNumeroTelephone
	 * @see ExceptionAdresseEmail
	 * @see ExceptionChampObligatoire 
	 * @throws  ExceptionNumeroTelephone   renvoit une erreur si le numero de telephone est incorrect
	 * @throws  ExceptionAdresseEmail   renvoit une erreur si l adresse email  est incorrecte
	 * @throws  ExceptionChampObligatoire     renvoit une erreur si un des champs a une valeur null ou vide 
	 */
	public Societe(String raisonSociale, EnumDomaine domaine, Adresse adresse, String telephone,
			String email) throws ExceptionNumeroTelephone, ExceptionAdresseEmail, ExceptionChampObligatoire {
		super();
		this.setRaisonSociale(raisonSociale);
		this.setDomaine(domaine);
		this.setAdresse(adresse);
		this.setTelephone(telephone);
		this.setEmail(email);
	}
	
	// GETTER ET SETTER
	public String getRaisonSociale() {
		return raisonSociale;
	}
	/**
	 * Envoit une exception de type ExceptionChampObligatoire si
	 *  la Raison Sociale passee en parametre a pour valeur 'null' ou 'chaine vide'
	 * @param raisonSociale String raisonSociale
	 * @throws ExceptionChampObligatoire ExceptionChampObligatoire
	 */
	public void setRaisonSociale(String raisonSociale) throws ExceptionChampObligatoire {
		if ((raisonSociale == null) || (raisonSociale.isEmpty())) {
			throw new ExceptionChampObligatoire("Le champs RaisonSociale est obligatoire");
		}else{
			this.raisonSociale = raisonSociale;
		}
	}
	public EnumDomaine getDomaine() {
		return domaine;
	}
	public void setDomaine(EnumDomaine domaine) {
		this.domaine = domaine;
	}
	public Adresse getAdresse() {
		return adresse;
	}
	/**
	 * Envoit une exception de type ExceptionChampObligatoire si
	 *  l'adresse passee en parametre a pour valeur 'null'
	 * @param  adresse Adresse 
	 * @throws ExceptionChampObligatoire exceptionChampObligatoire 
	 * @see Adresse
	 */
	public void setAdresse(Adresse adresse) throws ExceptionChampObligatoire {
		if ((adresse == null)) {
			throw new ExceptionChampObligatoire("Le champs Adresse est obligatoire");
		}else{
			this.adresse = adresse;
		}
	}
	public String getTelephone() {
		return telephone;
	}
	/**
	 * Envoit une excepton de type ExceptionChampObligatoire si la valeur 
	 *  du parametre 'telephone' est 'null' ou 'chaineVide'
	 * Envoit une exception de type ExceptionNumeroTelephone si le numero
	 *  de telephone ne respecte pas la Regex : (0|\\\\+33|0033)[1-9][0-9]{8}
	 * @param telephone String 
	 * @throws ExceptionNumeroTelephone exceptionNumeroTelephone 
	 * @throws ExceptionChampObligatoire exceptionChampObligatoire 
	 */
	public void setTelephone(String telephone) throws ExceptionNumeroTelephone, ExceptionChampObligatoire {
		if ((telephone == null) || (telephone.isEmpty())) {
			throw new ExceptionChampObligatoire("Le champs Telephone est obligatoire");
		}else{
			this.telephone = telephone;
		}
		Pattern p = Pattern.compile(VALID_NUM_TEL_REGEX);
		Matcher matcher = p.matcher(telephone);
		if (matcher.find()) {
			this.telephone = telephone;
		}else {
			throw new ExceptionNumeroTelephone("Numero de telephone incorrect.");
		}		
	}
	public String getEmail() {
		return email;
	}
	/**
	 * Envoit une excepton de type ExceptionChampObligatoire si la valeur 
	 *  du parametre 'email' est 'null' ou 'chaineVide'
	 * Envoit une exception de type ExceptionNumeroTelephone si l adresse email
	 *  ne respecte pas la Regex : \\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b
	 * @param  email String
	 * @throws ExceptionAdresseEmail exceptionAdresseEmail 
	 * @throws ExceptionChampObligatoire exceptionChampObligatoire 
	 */
	public void setEmail(String email) throws ExceptionAdresseEmail, ExceptionChampObligatoire {
		if ((email == null) || (email.isEmpty())) {
			throw new ExceptionChampObligatoire("Le champs Email est obligatoire");
		}else{
			this.email = email;
		}
		Pattern p = Pattern.compile(VALID_EMAIL_ADDRESS_REGEX);
		Matcher matcher = p.matcher(email);
		if (matcher.find()) {
			this.email = email;			
		}else {
			throw new ExceptionAdresseEmail("Adresse email incorrecte.");
		}
	}
	public String getCommentaires() {
		return commentaires;
	}
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}
	protected static Integer getNbrInstancesDeSociete() {
		return nbrInstancesDeSociete;
	}
	protected static void setNbrInstancesDeSociete() {
		Societe.nbrInstancesDeSociete++;
	}
	public static String[] getListeDesDonneesGerees() {
		return listeDesDonneesGerees;
	}
	public Integer getIdSociete() {
		return idSociete;
	}
	protected void setIdSociete(int idSociete) {
		this.idSociete = idSociete;
	}
	
	// TO_STRING
	@Override
	public String toString() {
		return this.raisonSociale + " "
				+ this.domaine + " "
				+ this.telephone + " "
				+ this.email + " " 
				+ this.adresse;
	}	
}
