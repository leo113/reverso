package fr.leo.metier;

import java.util.ArrayList;

import fr.leo.enums.EnumDomaine;
import fr.leo.exceptions.ExceptionAdresseEmail;
import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionChiffreAfaireInferieurZero;
import fr.leo.exceptions.ExceptionNombreEmployes;
import fr.leo.exceptions.ExceptionNumeroTelephone;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.exceptions.ExceptionRatioChiffreAfaireNombreEmployes;

public class Client extends Societe {
	
	/**
	 * Classe de gestion des Clients 
	 * C'est une sous classe concrete de la classe abstraite Societe 
	 * @see Societe
	 */
	
	// ATTRIBUTS DE CLASSE
	private static String[] listeDesDonneesGerees = {"idSociete", "raisonSociale", "domaine", "numRue" , "nomRue" ,
			"codePostal", "ville", "telephone", "email", "commentaire", "chiffreAfaire", "nbrEmployes" } ;
	private Integer nbrContratsClient = 0;

	// ATTRIBUTS D'INSTANCE
	private double chiffreAfaire;
	private int nbrEmployes;
	private ArrayList<Contrat> listeDesContratsDuClient;

	// CONSTRUCTEUR
	/**
	 * Envoit une exception de type ExceptionNumeroTelephone 					si le numero de telephone est incorrect
	 * Envoit une exception de type ExceptionAdresseEmail 						si l adresse email est incorrecte
	 * Envoit une exception de type ExceptionChampObligatoire					si un des 4 champs n'est pas renseigné
	 * Envoit une exception de type ExceptionPersonnalisee						si la date de prospection incorrecte
	 * Envoit une exception de type ExceptionChiffreAfaireInferieurZero			si le chiffre d affaire est inferieur strictement a zero
	 * Envoit une exception de type ExceptionNombreEmployes						si le nombre d employe est inferieur strictement a 1
	 * Envoit une exception de type ExceptionRatioChiffreAfaireNombreEmployes	si le ration entre le ChiffreAffaire
	 * 																			  et le NombreEmployes est inferieur stristement  a 10 
	 * 
	 * @param raisonSociale String 
	 * @param domaine EnumDomaine 
	 * @param adresse String 
	 * @param telephone String 
	 * @param email String 
	 * @param chiffreAfaire Double 
	 * @param nbreEmployes Integer 
	 * @throws ExceptionChiffreAfaireInferieurZero exceptionChiffreAfaireInferieurZero
	 * @throws ExceptionNombreEmployes exceptionNombreEmployes
	 * @throws ExceptionRatioChiffreAfaireNombreEmployes exceptionRatioChiffreAfaireNombreEmployes
	 * @throws ExceptionNumeroTelephone exceptionNumeroTelephone
	 * @throws ExceptionAdresseEmail exceptionAdresseEmail
	 * @throws ExceptionChampObligatoire exceptionChampObligatoire
	 * @see ExceptionChiffreAfaireInferieurZero
	 * @see ExceptionNombreEmployes
	 * @see ExceptionRatioChiffreAfaireNombreEmployes
	 * @see ExceptionNumeroTelephone
	 * @see ExceptionAdresseEmail
	 * @see ExceptionChampObligatoire
	 */
	public Client(String raisonSociale, EnumDomaine domaine, Adresse adresse, String telephone,
			String email, double chiffreAfaire, int nbreEmployes ) 
			throws ExceptionChiffreAfaireInferieurZero, ExceptionNombreEmployes,
			ExceptionRatioChiffreAfaireNombreEmployes, ExceptionNumeroTelephone, 
			ExceptionAdresseEmail, ExceptionChampObligatoire {
		super(raisonSociale, domaine, adresse, telephone, email);		
		this.setChiffreAfaire(chiffreAfaire);
		this.setNbrEmployes(nbreEmployes);
		this.setAdresse(adresse);		
		Societe.setNbrInstancesDeSociete();
		this.setIdSociete(Societe.getNbrInstancesDeSociete());
		calculRatioChiffreAfaireNombreEmployes();
		listeDesContratsDuClient = new ArrayList<Contrat>();
	}	
	
	// GETTERS ET SETTERS
	public ArrayList<Contrat> getListeDesContratsDuClient() {
		return listeDesContratsDuClient;
	}
	public Integer getNbrContratsClient() {
		return nbrContratsClient;
	}
	public void setNbrContratsClient(int nbr) {
		this.nbrContratsClient = this.nbrContratsClient + nbr;
	}	
	public Double getChiffreAfaire() {
		return chiffreAfaire ;
	}
	/**
	 * Renvoit une exception du type ExceptionChiffreAfaireInferieurZero si le 
	 *  chiffre affaire est inferieur a zero
	 * @param  chiffreAfaire  double
	 * @throws ExceptionChiffreAfaireInferieurZero exceptionChiffreAfaireInferieurZero
	 * @see ExceptionChiffreAfaireInferieurZero
	 */
	public void setChiffreAfaire(double chiffreAfaire) throws ExceptionChiffreAfaireInferieurZero {
		if (chiffreAfaire >= 0) {
			this.chiffreAfaire = chiffreAfaire;			
		}else{
			throw new ExceptionChiffreAfaireInferieurZero("Le chiffre d'afaire doit etre positif.");
		}
	}
	

	public Integer getNbrEmployes() {
		return nbrEmployes;
	}
	/**
	 * Renvoit une exception de type ExceptionNombreEmployes si le nombre d employes
	 *  est inferieur a 1
	 * @param nbrEmployes int
	 * @throws ExceptionNombreEmployes exceptionNombreEmployes
	 * @see ExceptionNombreEmployes
	 */
	public void setNbrEmployes(int nbrEmployes) throws ExceptionNombreEmployes {
		if (nbrEmployes > 0) {
			this.nbrEmployes = nbrEmployes;			
		}else{
			throw new ExceptionNombreEmployes("Le client doit avoir au minimum 1 employe.");
		}
	}
	
	/**
	 * Retourne la liste des donnees caracterisants un Client
	 *  et gerees par la classe
	 * @return String[] listeDesDonneesGerees
	 */
	public static String[] getListeDesDonneesGerees() {
		return listeDesDonneesGerees;
	}
	
	// METHODE calculRatioChiffreAfaireNombreEmployes
	/**
	 * Calcul le ration entre le ChiffreAffaire et le NombreEmployes
	 * Envoit une exception de type ExceptionRatioChiffreAfaireNombreEmployes si 
	 *  le ratio est inferieur a 10
	 * @throws ExceptionRatioChiffreAfaireNombreEmployes
	 */
	private void calculRatioChiffreAfaireNombreEmployes() throws ExceptionRatioChiffreAfaireNombreEmployes {
		if (this.chiffreAfaire/this.nbrEmployes <10) {
			throw new ExceptionRatioChiffreAfaireNombreEmployes("Le ratio entre "
					+ "le chiffre d'afaire et le nombre d'employes doit etre superieur e 10.");
		}
	}
	
	// METHODE addContratClient
	/**
	 * Ajoute un nouveau contrat à la liste des contrats du Client
	 * @param contrat Contrat
	 * @return ArrayList<Contrat> listeDesContratsDuClient
	 * @throws ExceptionPersonnalisee 
	 */
	public ArrayList<Contrat> addContratClient(Contrat contrat) throws ExceptionPersonnalisee{
		if (contrat == null) {
			throw new ExceptionPersonnalisee("Impossible d'ajouter un contrat null");
		}
		listeDesContratsDuClient.add(contrat);
		this.setNbrContratsClient(+1);
		return listeDesContratsDuClient;
	}
	// METHODE removeContratClient
	/**
	 * Supprime un contrat à la liste des contrats du Client
	 * @param contrat Contrat
	 * @return ArrayList<Contrat> listeDesContratsDuClient
	 */
	public ArrayList<Contrat> removeContratClient(Contrat contrat){
		listeDesContratsDuClient.remove(contrat);
		this.setNbrContratsClient(-1);
		return listeDesContratsDuClient;
	}

	// TO_STRING
	@Override
	public String toString() {
		return super.toString() + " " + this.chiffreAfaire + " " + this.getIdSociete() ;
	}
}
