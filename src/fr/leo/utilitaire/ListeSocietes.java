package fr.leo.utilitaire;

import java.util.ArrayList;
import java.util.Collections;

import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.metier.Societe;

public class ListeSocietes {
	
	/**
	 * Classe permettant de gerer un liste de Societe
	 * @see Societe
	 */

	// ATTRIBUTS DE CLASSE
	private static int nombreDeSocietes = 0;
	
	// ATTRIBUTS D'INSTANCE
	private ArrayList<Societe> listSocietes = new ArrayList<>();
				
	// CONSTRUCTEUR
	public ListeSocietes() {
	}
	
	// GETTERS ET SETTERS		
	/**
	 * Retourne une liste triee selon la RaisonSociale
	 * @return listSocietes ArrayList
	 */
	public ArrayList<Societe> getListSocietes() {
		Collections.sort(this.listSocietes, new ComparatorSociete());		
		return listSocietes;
	}	
	public static int getNombreDeSocietes() {
		return nombreDeSocietes;
	}
	private static void setNombreDeSocietes(int nombreDeSocietes) {
		ListeSocietes.nombreDeSocietes = nombreDeSocietes;
	}
	
					// *********** METHODES ***********	
	// SUPPRIMER_SOCIETE
	/**
	 * Supprime une societe de la liste.	
	 * Retourne une exception de type ExceptionPersonnalisee si la liste 
	 *  passe en parametre a pour valeur 'null'
	 * @param societe  Societe
	 * @return listeSociete ArrayList 
	 * @throws ExceptionPersonnalisee  exceptionPersonnalisee
	 * @see ExceptionPersonnalisee
	 */
	public ArrayList<Societe> supprimerSociete(Societe societe) throws ExceptionPersonnalisee {
		if (societe != null) {
			if (listSocietes.contains(societe)) {
				listSocietes.remove(societe);
				ListeSocietes.setNombreDeSocietes(--ListeSocietes.nombreDeSocietes);
				return listSocietes;
			}			
		}else{
			throw new ExceptionPersonnalisee("Impossible de supprimer une societe null");
		}
		return null;
	}
	
	// CHERCHER_SOCIETE
	/**
	 * Cherche une societe dans la liste.
	 * Retourne l'index de la societe passe en parametre dans la liste, si il est presente dans la liste.
	 * Retourne -1 si la societe ne figure pas dans la liste.
	 * @param _societe Societe 
	 * @return societeAChercher Societe 
	 */
	public int chercherSociete(Societe _societe){
		int indexListeSociete = listSocietes.indexOf(_societe);		
		return indexListeSociete;
	}
	
	// CHERCHER_SOCIETE PAR IDENTIFIANT
	/**
	 * Cherche une societe dans la liste.
	 * Retourne la reference a la societe dans la liste, si il est presente dans la liste.
	 * Retourne null si la societe ne figure pas dans la liste.
	 * @param  Idsociete int 
	 * @return Societe societeAChercher
	 */
	public Societe chercherSocieteParIdentifant(int Idsociete){
		for (Societe societe : listSocietes) {
			if (societe.getIdSociete() == Idsociete) {
				return societe;
			}
		}		
		return null;
	}
	

	// AJOUTER_SOCIETE
	/**
	 * Ajoute une societe a la liste des societe.
	 * Envoit une exception de type ExceptionPersonnalisee si la
	 *  liste passee en parametre a pour valeur 'null' 
	 *  ou si la liste passee en parametre est deja presente dans la liste
	 * @param societe  Societe
	 * @return listeSociete  ArrayList 
	 * @throws ExceptionPersonnalisee  exceptionPersonnalisee
	 * @see ExceptionPersonnalisee
	 */
	public ArrayList<Societe> ajouterSociete(Societe societe) throws  ExceptionPersonnalisee {
		if (societe != null) {
			if (this.listSocietes.contains(societe)) {
				throw new ExceptionPersonnalisee("Impossible d'ajouter dans la liste une societe deja presente dans la liste");
			}else{
			listSocietes.add(societe);
			ListeSocietes.setNombreDeSocietes(++ListeSocietes.nombreDeSocietes);
			}
		}else{
			throw new ExceptionPersonnalisee("Impossible d'ajouter dans la liste une societe avec la valeur Null");
		}
		return listSocietes;
	}	

}
